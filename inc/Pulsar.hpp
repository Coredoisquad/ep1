#ifndef PULSAR_HPP
#define PULSAR_HPP
#include "Grade_Do_Jogo.hpp"

using namespace std;

class Pulsar : public Grade{

public:
  Pulsar();

  void PulsaR();
  void PulsaR(int linha, int coluna);

};

#endif
