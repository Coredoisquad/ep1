#ifndef GLIDER_HPP
#define GLIDER_HPP
#include "Grade_Do_Jogo.hpp"

using namespace std;

class Glider : public Grade{

public:
  Glider();

  void GlideR();
  void GlideR(int linha, int coluna);

};

#endif
