#include <iostream>
#include "Grade_Do_Jogo.hpp"

using namespace std;

Grade::Grade(){
}

void Grade::setMatriz(int linha, int coluna){
  Matriz[linha][coluna] = true;
}

int Grade::checar_Vivos(int linha, int coluna){
  int i, j, qnt_vivos=0;

  for(i=(linha-1);i<(linha+2);i++){
    for(j=(coluna-1);j<(coluna+2);j++){
      if(Matriz_copia[i][j] == true){
        qnt_vivos += 1;
      }
    }
  }
  if(Matriz_copia[linha][coluna] == true){
    qnt_vivos -= 1;
  }
  return qnt_vivos;
}

void Grade::checar_regras(){
  int linha=0, coluna=0, qnt_vivos;

  copiar_Matriz();
  for(linha=1;linha<35;linha++){
    for(coluna=1;coluna<39;coluna++){
      qnt_vivos = 0;
      qnt_vivos = checar_Vivos(linha, coluna);
      if(qnt_vivos < 2){
        Matriz[linha][coluna] = false;
      }
      else if(qnt_vivos > 3){
        Matriz[linha][coluna] = false;
      }
      else if(qnt_vivos == 3){
        Matriz[linha][coluna] = true;
      }
    }
  }
}


void Grade::imprimir(){

  for(linha=1;linha<36;linha++){
    for(coluna=1;coluna<40;coluna++){
      if(Matriz[linha][coluna] == true){
        cout << "0" << " ";
      }
      else if(Matriz[linha][coluna] == false){
        cout << "." << " ";
      }
      if(coluna == 38){
        cout << "|" << endl;
      }
    }
  }
}

void Grade::copiar_Matriz(){
    for(linha=0;linha<36;linha++){
      for(coluna=0;coluna<40;coluna++){
        if(Matriz[linha][coluna] == true){
          Matriz_copia[linha][coluna] = true;
        }
        else{
          Matriz_copia[linha][coluna] = false;
        }
      }
    }
}

void Grade::extermina_Matriz(){
  for(linha=0;linha<36;linha++){
    for(coluna=0;coluna<40;coluna++){
      Matriz_copia[linha][coluna] = false;
      Matriz[linha][coluna] = false;
    }
  }
}
