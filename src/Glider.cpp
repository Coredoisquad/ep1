#include <iostream>
#include <stdlib.h>

#include "Glider.hpp"

using namespace std;

Glider::Glider(){

}

void Glider::GlideR(){

  setMatriz(1,2);
  setMatriz(2,3);
  setMatriz(3,1);
  setMatriz(3,2);
  setMatriz(3,3);

}

void Glider::GlideR(int linha, int coluna){

      setMatriz(linha+1,coluna+2);
      setMatriz(linha+2,coluna+3);
      setMatriz(linha+3,coluna+1);
      setMatriz(linha+3,coluna+2);
      setMatriz(linha+3,coluna+3);

}
